import unittest
from gridChallenge import gridChallenge


class GridChallengeTest(unittest.TestCase):
    def test_efotx_is_yes(self):
        grid = ['eabcd',
                'fghij',
                'olkmn',
                'trpqs',
                'xywuv']
        is_YES = gridChallenge(grid)
        self.assertEqual(is_YES,"YES")

    def test_maw_is_no(self):
        grid = ['mpxz',
                'abcd',
                'wlmf']
        is_NO = gridChallenge(grid)
        self.assertEqual(is_NO,"NO")

    def test_alq_is_yes(self):
        grid = ['abc',
                'lmp',
                'qrt']
        is_YES = gridChallenge(grid)
        self.assertEqual(is_YES,"YES")
