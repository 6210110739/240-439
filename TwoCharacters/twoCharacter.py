def alternate(s):
    sSet = set(s)
    sList = list(sSet)

    i = 0
    altC = []
    for i in range(len(sList)):
        c = i+1
        while(c<len(sList)):
            altC.append([sList[i],sList[c]])
            c += 1

    altStringList = []
    for altSet in altC:
        mapList = sList.copy()
        mapList.remove(altSet[0])
        mapList.remove(altSet[1])
        removalMap = {}
        for c in mapList:
            removalMap[ord(c)] = None

        altString = s.translate(removalMap)
        altStringList.append(altString)

    c = 0
    length = len(altStringList)
    while c <  length:
        for i in range(len(altStringList[c])-1):
            if(altStringList[c][i] == altStringList[c][i+1]):
                altStringList.pop(c)
                c -=1
                length -=1
                break
        c += 1
    if len(altStringList) == 0:
        return(0)
    else:
        return(max([len(s) for s in altStringList]))

"""
if __name__ == '__main__':
    line_str = input("Enter:")
    line = map(str,line_str.split())

    result = alternate(*line)
    print("Result:", result)
"""
