import unittest
from twoCharacter import alternate


class TwoCharacterTest(unittest.TestCase):
    def test_beabeefeab_is_5(self):
        s = "beabeefeab"
        is_5 = alternate(s)
        self.assertEqual(is_5,5)

    def test_asdcbsdcagfsdbgdfanfghbsfdab_is_8(self):
        s = "asdcbsdcagfsdbgdfanfghbsfdab"
        is_8 = alternate(s)
        self.assertEqual(is_8,8)

    def test_asvkugfiugsalddlasguifgukvsa_is_0(self):
        s = "asvkugfiugsalddlasguifgukvsa"
        is_0 = alternate(s)
        self.assertEqual(is_0,0)
