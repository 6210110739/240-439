def caesarcipher(s,k):
    newText=""
    for letter in s:
        if letter in "abcdefghijklmnopqrstuvwxyz":
            newText +=chr(97+(ord(letter)-97+k)%26)
        elif letter in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
            newText +=chr(65+(ord(letter)-65+k)%26)
        else: newText += letter
    return newText
