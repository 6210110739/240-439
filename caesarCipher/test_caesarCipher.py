import unittest
from caesar_Cipher import caesarcipher

class FunnyStringTest(unittest.TestCase):
    def test_give_Always_is_Fqbfdx(self):
        is_Fqbfdx = caesarcipher("Always-Look-on-the-Bright-Side-of-Life",5)
        self.assertEqual(is_Fqbfdx,"Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj")

    def test_give_middle_Outz_is_okffng_Qwvb(self):
        is_okffng_Qwvb = caesarcipher("middle-Outz",2)
        self.assertEqual(is_okffng_Qwvb,"okffng-Qwvb")
