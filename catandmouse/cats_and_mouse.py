def catandmouse(x,y,z):
    catch_mouse_catA = abs(z-x)
    catch_mouse_catB = abs(z-y)

    if catch_mouse_catA == catch_mouse_catB:
        return "Mouse C"
    if catch_mouse_catA < catch_mouse_catB:
        return "Cat A"
    if catch_mouse_catA > catch_mouse_catB:
        return "Cat B"

"""
if __name__ == '__main__':
    ##line_str = input("Enter A B C:")
    ##line = map(int,line_str.split())

    result = catandmouse(*line)
    print("Result:", result)
"""