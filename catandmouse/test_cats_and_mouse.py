import unittest
from cats_and_mouse import catandmouse


class CatsAndMouseTest(unittest.TestCase):
    def test_give_2_5_4_is_CatB(self):
        is_CatB = catandmouse(2,5,4)
        self.assertEqual(is_CatB,"Cat B")

    def test_give_8_6_10_is_CatA(self):
        is_CatA = catandmouse(8,6,10)
        self.assertEqual(is_CatA,"Cat A")

    def test_give_3_11_7_is_MouseC(self):
        is_MouseC = catandmouse(3,11,7)
        self.assertEqual(is_MouseC,"Mouse C")
