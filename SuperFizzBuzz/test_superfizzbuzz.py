import unittest
from superfizzbuzz import ModSuperFizzBuzz


class SuperFizzBuzzTest(unittest.TestCase):
    def test_give_negative15_is_fizz(self):
        num = -15
        is_nofizzbuzz = ModSuperFizzBuzz.nofizzbuzz(num)
        self.assertEqual(is_nofizzbuzz,"NoFizzBuzz")

    def test_give_1_is_fizz(self):
        num = 1
        is_nofizzbuzz = ModSuperFizzBuzz.nofizzbuzz(num)
        self.assertEqual(is_nofizzbuzz,"NoFizzBuzz")

    def test_give_3_is_fizz(self):
        num = 3
        is_fizz = ModSuperFizzBuzz.fizz(num)
        self.assertEqual(is_fizz,"Fizz")

    def test_give_5_is_buzz(self):
        num = 5
        is_buzz = ModSuperFizzBuzz.buzz(num)
        self.assertEqual(is_buzz,"Buzz")

    def test_give_15_is_fizzbuzz(self):
        num = 15
        is_fizzbuzz = ModSuperFizzBuzz.fizzbuzz(num)
        self.assertEqual(is_fizzbuzz,"FizzBuzz")

    def test_give_9_is_fizzfizz(self):
        num = 9
        is_fizzfizz = ModSuperFizzBuzz.fizzfizz(num)
        self.assertEqual(is_fizzfizz,"FizzFizz")

    def test_give_25_is_buzzbuzz(self):
        num = 25
        is_buzzbuzz = ModSuperFizzBuzz.buzzbuzz(num)
        self.assertEqual(is_buzzbuzz,"BuzzBuzz")

    def test_give_225_is_fizzfizzbuzzbuzz(self):
        num = 225
        is_fizzfizzbuzzbuzz = ModSuperFizzBuzz.fizzfizzbuzzbuzz(num)
        self.assertEqual(is_fizzfizzbuzzbuzz,"FizzFizzBuzzBuzz")

    def test_give_5000_is_fizz(self):
        num = 50000
        is_buzz= ModSuperFizzBuzz.buzz(num)
        self.assertEqual(is_buzz,"Buzz")

    def test_give_9999_is_fizz(self):
        num = 9999
        is_fizz = ModSuperFizzBuzz.fizz(num)
        self.assertEqual(is_fizz,"Fizz")

    def test_give_11111_is_nofizzbuzz(self):
        num = 11111
        is_nofizzbuzz = ModSuperFizzBuzz.nofizzbuzz(num)
        self.assertEqual(is_nofizzbuzz,"NoFizzBuzz")
