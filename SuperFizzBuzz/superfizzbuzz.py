class Result:
    def __init__(self, numbers: int):
        self.numbers = numbers


class ModSuperFizzBuzz(Result):

    def fizz(self):
        if self % 3 == 0:
            return "Fizz"

    def buzz(self):
        if self % 5 == 0:
            return "Buzz"

    def fizzbuzz(self):
        if self % 3 == 0 and self % 5 == 0:
            return "FizzBuzz"

    def fizzfizz(self):
        if self % 9 == 0:
            return "FizzFizz"

    def buzzbuzz(self):
        if self % 25 == 0:
            return "BuzzBuzz"

    def fizzfizzbuzzbuzz(self):
        if self % 9 == 0 and self % 25 == 0:
            return "FizzFizzBuzzBuzz"

    def nofizzbuzz(self):
        if ((self % 3 and self % 5 and self % 9 and self % 25) != 0) <= 1:
            return "NoFizzBuzz"
