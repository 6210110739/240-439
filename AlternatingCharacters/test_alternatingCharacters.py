import unittest
from alternatingCharacters import alternatingCharacters


class AlternatingCharactersTest(unittest.TestCase):
    def test_give_aaaa_is_3(self):
        is_3 = alternatingCharacters("aaaa")
        self.assertEqual(is_3,3)

    def test_give_BBBBB_is_4(self):
        is_4 = alternatingCharacters("BBBBB")
        self.assertEqual(is_4,4)

    def test_give_ABABABAB_is_0(self):
        is_0 = alternatingCharacters("ABABABAB")
        self.assertEqual(is_0,0)

    def test_give_BABABA_is_0(self):
        is_0 = alternatingCharacters("BABABA")
        self.assertEqual(is_0,0)

    def test_give_AAABBB_is_4(self):
        is_4 = alternatingCharacters("AAABBB")
        self.assertEqual(is_4,4)
