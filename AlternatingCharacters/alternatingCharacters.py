def alternatingCharacters(s):
    count=0
    for i in range(0,len(s)-1):
        if s[i]==s[i+1]:
            count = count+1
    return count

"""
if __name__ == '__main__':
    line_str = input("Enter:")
    line = map(str,line_str.split())

    result = alternatingCharacters(*line)
    print("Result:", result)
"""
