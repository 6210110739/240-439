import unittest
from FunnyString import funnyString


class funnystringTest(unittest.TestCase):
    def test_give_acxz_is_Funny(self):
        is_Funny = funnyString("acxz")
        self.assertEqual(is_Funny,"Funny")

    def test_give_bcxz_is_NotFunnyB(self):
        is_NotFunnyB = funnyString("bcxz")
        self.assertEqual(is_NotFunnyB,"Not Funny")
